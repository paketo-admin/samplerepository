package com.example.demo.controller;

import com.example.demo.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hashworks-60 on 22/5/17.
 */
@RestController
@RequestMapping(value = "/api/timesheet/manager")
public class ManagerController {

    @Autowired
    private ManagerService managerService;

    @RequestMapping(value = "approve/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
   public ResponseEntity<?> approveTimesheet(@PathVariable long id) {
        Map responseMap=new HashMap();
        HttpStatus status=HttpStatus.OK;
        if(this.managerService.approveTimesheet(id)) {
            responseMap.put("message", "timesheet approved");
        }else {
            status=HttpStatus.INTERNAL_SERVER_ERROR;
            responseMap.put("message","failed to save");
        }
        return new ResponseEntity<Object>(responseMap,status);

    }


    }
