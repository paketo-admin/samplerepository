package com.example.demo.service;

import com.example.demo.model.Timesheet;

import javax.transaction.Transactional;

/**
 * Created by hashworks-60 on 22/5/17.
 */
@Transactional
public interface ManagerService {
    public boolean approveTimesheet(long id);
}
