package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by hashworks-60 on 19/5/17.
 */
@Entity
@Table(name = "timesheet")
public class Timesheet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private Date createdDate;

    @Getter
    @Setter
    private Date updatedDate;

    @Getter
    @Setter
    private Double workingHours;

    @Getter
    @Setter
    private boolean status;


    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "emp_id")
    @JsonBackReference
    private Employee employee;
}
