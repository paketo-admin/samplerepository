package com.example.demo.service;

import com.example.demo.model.Employee;
import com.example.demo.model.Timesheet;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by hashworks-60 on 22/5/17.
 */
@Transactional
public interface EmployeeService {
    public List viewTimesheet (Long Id);
    public boolean addEmployee(Employee employeeSetup) ;
}
