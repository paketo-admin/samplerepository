package com.example.demo.controller;
import com.example.demo.model.Timesheet;
import com.example.demo.service.TimesheetService;
import com.fasterxml.jackson.databind.util.JSONPObject;
import jdk.nashorn.internal.runtime.JSONFunctions;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hashworks-60 on 19/5/17.
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/timesheet/new/")
public class TimesheetController {
    @Autowired
    private TimesheetService timesheetService;

    @RequestMapping(value = "submit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> submitTimesheet(@RequestBody String str) {
        Map responseMap = new HashMap();
        HttpStatus status = HttpStatus.OK;
        if (this.timesheetService.submitTimesheet(str)) {
            responseMap.put("message", "successfull");
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            responseMap.put("message", "failed");
        }
        return new ResponseEntity<Object>(responseMap,status);
    }

}




