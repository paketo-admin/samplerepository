package com.example.demo.controller;

import com.example.demo.service.ListScmApiService;
import com.example.demo.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Tapan on 21/09/17.
 */


//

@RestController
//@RequestMapping(value = "")


public class ScmRestApiController {

    @Autowired
    private ListScmApiService listScmApiService;

    @ResponseBody
    public ResponseEntity<List> getScmApi()
    {
        HttpStatus status = HttpStatus.OK;
        List responseList = new ArrayList();
        responseList = listScmApiService.getScmApi();
//        log.info("API list "+responseList);
        return new ResponseEntity<List>(responseList,status);

    }


}
