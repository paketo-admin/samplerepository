package com.example.demo.service.bean;

import com.example.demo.model.Employee;
import com.example.demo.model.Timesheet;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.repository.TimesheetRepository;
import com.example.demo.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hashworks-60 on 22/5/17.
 */
@Slf4j
@Service
public class EmployeeBean implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TimesheetRepository timesheetRepository;

    public List viewTimesheet(Long Id) {
        Employee employee = this.employeeRepository.findById(Id);
        List<Timesheet> timesheets = this.timesheetRepository.findAllByEmployee(employee);
        return timesheets;
    }

    @Override
    public boolean addEmployee(Employee employeeSetup) {
        try {
            employeeRepository.save(employeeSetup);
            log.info("Saved sucessfully");
            return true;
        } catch (Exception e) {
            log.error("Error while saving Employee", e);
            return false;
        }
    }
}