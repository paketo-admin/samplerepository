package com.example.demo.repository;

import com.example.demo.model.Timesheet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by hashworks-60 on 22/5/17.
 */
@Repository
public interface ManagerRepository extends JpaRepository<Timesheet, Long> {
}
