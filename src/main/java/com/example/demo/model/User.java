package com.example.demo.model;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by abhishek on 26/5/17.
 */
@Entity
@Table(name="users")

public class User {


        @Setter
        @Getter
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Long id;

        @Id
        @Setter
        @Getter
        private Long userId;


        private String userPassword;

        public void setPassword(String userPassword)
        {
            this.userPassword=userPassword;
        }

    public String getPassword()
    {
        return userPassword;
    }

        @Setter
        @Getter
        @OneToOne
        @JoinColumn(name="emp_id")
        private Employee employee;
}
