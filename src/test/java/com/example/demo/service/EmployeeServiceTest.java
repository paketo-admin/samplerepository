package com.example.demo.service;

import com.example.demo.model.Employee;
import org.apache.catalina.Manager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by abhishek on 26/5/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceTest {
    @Autowired
    private EmployeeService employeeService;

    public void save_employee()

    {
        Employee employee = new Employee();
        employee.setId(107l);
        employee.setEmployee_name("Abhi");
        employee.setUser_name("abhi123");
        employee.setPassword("123456");
        boolean data = this.employeeService.addEmployee(employee);
        Assert.assertNotNull(data);
        Assert.assertEquals(true, data);
    }

}