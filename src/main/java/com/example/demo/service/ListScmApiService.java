package com.example.demo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

@Transactional
@Service
public class ListScmApiService {

    @Value("${bit.repository.url}")
    private String bitBucketUrl;

    @Value("${bit.branches.url}")
    private String bitBranchUrl;

    public List getScmApi() {
        RestTemplate restTemplate = new RestTemplate();
        List responsList = new ArrayList();
        Map result = restTemplate.getForObject(bitBucketUrl, Map.class);
        List<Map<?, ?>> values = (List<Map<?, ?>>) result.get("values");
        for (Map<?, ?> item : values) {
            Map data = new HashMap();
//            data.put("SCM", item.get("scm"));
            data.put("Name", item.get("name"));

            //Take username and type values from "owner"(this is listed inside values)
            Map<?, ?> owner = (Map<?, ?>) item.get("owner");
            data.put("UserName", owner.get("username"));
            data.put("Type", owner.get("type"));

            //Take repository URL value from "links"->"self"(this is listed inside values)
            Map<?, ?> links = (Map<?, ?>) item.get("links");
            Map<?, ?> self = (Map<?, ?>) links.get("html");
            data.put("RepositoryUrl", self.get("href"));
            String bitbucket = (String) self.get("href");
            String bitBucketOutput;
//            System.out.println(bitbucket);
            Pattern regex = Pattern.compile("[^/.]+(?=\\.[^.]+$)");
            Matcher regexMatcher = regex.matcher(bitbucket);
            if (regexMatcher.find()) {
                bitBucketOutput = regexMatcher.group();
//                System.out.println(bitBucketOutput);
                data.put("scm",bitBucketOutput);

            }

            responsList.add(data);


        }
        return (responsList);
    }

    public List getBranchesApi() {
        RestTemplate restTemplate = new RestTemplate();
        List branchesList = new ArrayList();
        Map result = restTemplate.getForObject(bitBranchUrl, Map.class);
        Map<?, ?> test= (Map<?, ?>) result.get("test");
        List branches = (List) test.get("branches");
        System.out.println(branches);
        return branches;

//        for (Map<?, ?> item : test) {
          /*  Map data = new HashMap();
            data.put("SCM", item.get("scm"));
            data.put("Name", item.get("name"));

            //Take username and type values from "owner"(this is listed inside values)
            Map<?, ?> owner = (Map<?, ?>) item.get("owner");
            data.put("UserName", owner.get("username"));
            data.put("Type", owner.get("type"));

            //Take repository URL value from "links"->"self"(this is listed inside values)
            Map<?, ?> links = (Map<?, ?>)item.get("links");
            Map<?, ?> self = (Map<?, ?>)links.get("self");
            data.put("RepositoryUrl", self.get("href"));*/

//            branchesList.add(data);



//        return (branchesList);



    }

}
