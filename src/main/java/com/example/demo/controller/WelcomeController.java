package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

/**
 * Created by abhishek on 30/5/17.
 */
@Controller
@Slf4j
public class WelcomeController {

    /*@RequestMapping("/")
    public String home() {
        return "home";
    }*/

    @RequestMapping("/")
    public String signIn() {
        return "login";
    }

    @RequestMapping("/signOut")
    public String signOut(HttpServletRequest request, HttpServletResponse response) {
        request.getSession().invalidate();

        //to remove data from cookie
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("data")) {
                    cookie.setValue(null);
                    cookie.setMaxAge(0);
                    response.addCookie(cookie);
                }
            }
        }
        return "login";
    }

    @RequestMapping("/welcome")
    public String welcome(Map model, HttpServletRequest request) {
       /* try {
            Cookie[] cookies = request.getCookies();

            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("data")) {
                        String value = URLDecoder.decode(cookie.getValue(), "UTF-8");
                        JSONObject jsonObject = new JSONObject(value);
                        jsonObject = jsonObject.getJSONObject("data");
                        model.put("name", jsonObject.getString("name"));
                        model.put("email", jsonObject.getString("email"));
                        model.put("mobile", jsonObject.getString("mobile"));
                        model.put("address", jsonObject.getString("address"));
                    }
                }
            }
            return "welcome";
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
            model.put("status", "failure");
            model.put("errors", "Invalid Input");
            model.put("loginError", true);*/
            return "welcome";
        }

    }

