package com.example.demo.service.bean;

import com.example.demo.model.Employee;
import com.example.demo.model.Timesheet;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.repository.TimesheetRepository;
import com.example.demo.service.TimesheetService;
import javassist.bytecode.stackmap.BasicBlock;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Created by hashworks-60 on 19/5/17.
 */
@Slf4j
@Service
public class TimesheetBean implements TimesheetService {

    @Autowired
    private TimesheetRepository timesheetRepository;

    @Autowired
    private EmployeeRepository employeeRepository;


    public boolean submitTimesheet(String timesheet) {
        try {
            Timesheet timesheet1 = new Timesheet();
            JSONObject json = new JSONObject(timesheet);
            Long empId = json.getLong("empId");
            if (empId != null && empId > 0)
            {
                Employee employee=this.employeeRepository.findById(empId);
                //Employee employee = this.employeeRepository.findOne(empId);
                timesheet1.setEmployee(employee);
                timesheet1.setCreatedDate(new Date());
                timesheet1.setUpdatedDate(new Date());
                timesheet1.setWorkingHours(json.getDouble("workingHours"));
                timesheet1.setStatus(json.getBoolean("status"));
                //convert the string to json
                //collect all the values from json one by one and set that to timesheet
                //take the emplyee id and find the emp object
                // set that emp ob to time seet
                // save the time sheet
                timesheetRepository.save(timesheet1);
                return true;
            }
            else {
                return false;

            }
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }
}